package Demo.DemoBlaze;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import Demo.PageFactory.DemoBlaze.Login;
import Demo.PageFactory.DemoBlaze.PlaceOrder;
import Demo.utilities.Browser;
import Demo.utilities.Utility;
import Demo.utilities.WebDriverFactory;

public class PlaceOrderTest {
	private WebDriver driver;
	ExtentTest parentTest;

	Login login;
	PlaceOrder placeOrder;
    private Utility util = new Utility();
    String testName = "";
    String RunTest = "";
    String name = "";
    String productCatgory = "";
    String productName = "";
    String country = "";
    String city = "";
    String CardNumber = "";
    String month = "";
    String year = "";
    
    @BeforeTest
	public void setup() throws Exception{
    	
     	
    	Browser.initialize(WebDriverFactory.CHROME);
        
        driver = Browser.Driver();
        
        String className = this.getClass().getName();
        util.createReport();
        parentTest = util.createTest(className);
        
	        
	        
	}
    
    @Test
    public void place_order_feature() throws Exception{
    	
        //Create Login Page object
    	placeOrder = new PlaceOrder(driver, util);
    	
    	
    	int rowSize = util.getRowSize("BlazeDemo","PlaceOrder");
    	
    	for(int i = 1; i < rowSize; i++) {
    		
    		loadData(i);
    		Browser.goTo("https://demoblaze.com/");
    		
    		RunTest = util.getCellData("BlazeDemo","PlaceOrder","RunTest",i);
    		
    		if(RunTest.equalsIgnoreCase("True")) {
        		
        		
        		testName = util.getCellData("BlazeDemo","PlaceOrder","Test name",i);
        		util.createNode(parentTest, testName);
        		

        		 
        		placeOrder.placeOrder(name, productCatgory, productName, country, city, CardNumber, month, year);
    		}

    	}
	    
	    
	    

    }
    
    @AfterTest
    public void AterTest() {
    	
    	util.getInstance().flush();
    	Browser.close();
    }
    
    public void loadData(int i) throws Exception {
    	
		name = util.getCellData("BlazeDemo","PlaceOrder","Name",i);
		productCatgory = util.getCellData("BlazeDemo","PlaceOrder","Product catgory",i);
		productName = util.getCellData("BlazeDemo","PlaceOrder","Product name",i);
		country = util.getCellData("BlazeDemo","PlaceOrder","Country",i);
		city = util.getCellData("BlazeDemo","PlaceOrder","City",i);
		CardNumber = util.getCellData("BlazeDemo","PlaceOrder","Card Number",i);
		month = util.getCellData("BlazeDemo","PlaceOrder","Month",i);
		year = util.getCellData("BlazeDemo","PlaceOrder","Year",i);
		
    }
    
}
