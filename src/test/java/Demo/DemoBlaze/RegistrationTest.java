package Demo.DemoBlaze;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;


import Demo.PageFactory.DemoBlaze.Registration;
import Demo.utilities.*;
public class RegistrationTest {
	private WebDriver driver;
	
	ExtentTest parentTest;

	Registration register;
    private Utility util = new Utility();
   
    
    @BeforeTest
	public void setup() throws Exception{
    	
     	//obj.getCellData("BlazeDemo","Registration","TestID",0);
    	Browser.initialize(WebDriverFactory.FIREFOX);
        Browser.goTo("https://demoblaze.com/");
        driver = Browser.Driver();
        
        String className = this.getClass().getName();
        util.createReport();
        parentTest = util.createTest(className);
        
	        
	        
	}
    
    @Test
    public void test_registration_feature() throws IOException, InterruptedException{
    	
        //Create Login Page object
    	register = new Registration(driver, util);
	    util.createNode(parentTest,"Registration");
	    
	    register.register("Lebo", "12345");

    }
    
    @AfterTest
    public void AterTest() {
    	
    	util.getInstance().flush();
    	Browser.close();
    }
    


}
