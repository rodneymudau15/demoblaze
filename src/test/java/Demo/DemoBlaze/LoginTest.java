package Demo.DemoBlaze;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import Demo.PageFactory.DemoBlaze.Login;
import Demo.PageFactory.DemoBlaze.Registration;
import Demo.utilities.Browser;
import Demo.utilities.Utility;
import Demo.utilities.WebDriverFactory;

public class LoginTest {
private WebDriver driver;
	
	ExtentTest parentTest;

	Login login;
    private Utility util = new Utility();
   
    
    @BeforeTest
	public void setup() throws Exception{
    	
     	//obj.getCellData("BlazeDemo","Registration","TestID",0);
    	Browser.initialize(WebDriverFactory.CHROME);
        Browser.goTo("https://demoblaze.com/");
        driver = Browser.Driver();
        
        String className = this.getClass().getName();
        util.createReport();
        parentTest = util.createTest(className);
        
	        
	        
	}
    
    @Test
    public void test_login_feature() throws IOException, InterruptedException{
    	
        //Create Login Page object
    	login = new Login(driver, util);
	    util.createNode(parentTest,"Login Test");
	    
	    login.login("khumbe12", "12345");

    }
    
    @AfterTest
    public void AterTest() {
    	
    	util.getInstance().flush();
    	Browser.close();
    }
    

}
