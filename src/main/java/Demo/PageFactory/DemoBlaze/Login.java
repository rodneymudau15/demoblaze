package Demo.PageFactory.DemoBlaze;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Demo.utilities.Utility;

public class Login {
	/**

     * All WebElements are identified by @FindBy annotation
 
     */

    WebDriver driver;
    Utility util;
  
    @FindBy(xpath="//*[@id='login2']")
    WebElement loginLink;

  
    @FindBy(xpath="//*[@id='loginusername']")
    WebElement usernameField;    
  
    @FindBy(xpath="//*[@id='loginpassword']")
    WebElement passwordField;    
  
    @FindBy(xpath="//*[@id='logInModal']/div/div/div[3]/button[2]")
    WebElement loginButton;  
    
    public Login(WebDriver driver, Utility util){

        this.driver = driver;
        this.util = util;
        //This initElements method will create all WebElements

        PageFactory.initElements(driver, this);
    }
    
    //Click on login links

    public void clickLoginLink(){
    	
    	Utility.clickElement(loginLink, this.driver, this.util);

    }  
    
    //Set user name in field

    public void setUserName(String strUserName) throws IOException{
 
    	Utility.sendKeys(usernameField, driver, strUserName, this.util);
    }

    //Set password in password field

    public void setPassword(String strPassword) throws IOException{

    	Utility.sendKeys(passwordField, driver, strPassword, this.util);
    }

    //Click on login button

    public void clickLoginButton(){
    	
    	Utility.clickElement(loginButton, this.driver, this.util);

    }  

    //Get the title of Login Page

    public String getMessage() throws InterruptedException, IOException{

    	return  Utility.handlePopUp(this.driver, this.util, loginButton);

    }
    
    /**

     * This POM method will be exposed in test case to login in the application

     * @param strUserName

     * @param strPasword

     * @return
     * @throws InterruptedException 
     * @throws IOException 

     */

    public void login(String strUserName,String strPasword) throws InterruptedException, IOException{
    
    	//Fill user name
        this.clickLoginLink();
        
        //Fill user name
        this.setUserName(strUserName);
        
        //Fill password
        this.setPassword(strPasword);
      
        //Click login button
         this.clickLoginButton();           
       
        
    }
}
