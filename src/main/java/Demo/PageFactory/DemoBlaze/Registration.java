package Demo.PageFactory.DemoBlaze;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Demo.utilities.Utility;

public class Registration {
	/**

     * All WebElements are identified by @FindBy annotation
 
     */

    WebDriver driver;
    Utility util;
    @FindBy(xpath="//*[@id='signin2']")
    WebElement signUpLink;

  
    @FindBy(xpath="//*[@id='sign-username']")
    WebElement usernameField;    
  
    @FindBy(xpath="//*[@id='sign-password']")
    WebElement passwordField;    
  
    @FindBy(xpath="//*[@id='signInModal']/div/div/div[3]/button[2]")
    WebElement signUpButton;  
    
    public Registration(WebDriver driver, Utility util){

        this.driver = driver;
        this.util = util;
        //This initElements method will create all WebElements

        PageFactory.initElements(driver, this);
    }
    
    //Click on sign-up links

    public void clickSignUpLink(){
    	
    	Utility.clickElement(signUpLink, this.driver, this.util);

    }  
    
    //Set user name in field

    public void setUserName(String strUserName) throws IOException{
 
    	Utility.sendKeys(usernameField, driver, strUserName, this.util);
    }

    //Set password in password field

    public void setPassword(String strPassword) throws IOException{

    	Utility.sendKeys(passwordField, driver, strPassword, this.util);
    }

    //Click on login button

    public void clickSignUp(){
    	
    	Utility.clickElement(signUpButton, this.driver, this.util);

    }  

    //Get the title of Login Page

    public String getMessage() throws InterruptedException, IOException{

    	return  Utility.handlePopUp(this.driver, this.util, signUpButton);

    }
    
    /**

     * This POM method will be exposed in test case to login in the application

     * @param strUserName

     * @param strPasword

     * @return
     * @throws InterruptedException 
     * @throws IOException 

     */

    public void register(String strUserName,String strPasword) throws InterruptedException, IOException{
    
    	//Fill user name
        this.clickSignUpLink();
        
        //Fill user name
        this.setUserName(strUserName);
        
        //Fill password
        this.setPassword(strPasword);
      
        //Click SignUp button
       // this.clickSignUp();           
       
        this.getMessage();
    }
}
