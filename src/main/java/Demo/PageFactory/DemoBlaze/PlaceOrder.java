package Demo.PageFactory.DemoBlaze;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import Demo.utilities.Utility;

public class PlaceOrder {
	/**

     * All WebElements are identified by @FindBy annotation
 
     */

    WebDriver driver;
    Utility util;
    String name = "";
    String productCatgory = ""; 
    String productName = ""; 
    String country = "";
    String city = "";
    String CardNumber = "";
    String month = "";
    String year = "";
  
    @FindBys(@FindBy(xpath="//*[@id='tbodyid']/div//div/div/h4/a"))
    List <WebElement> products;
    
    @FindBy(xpath="//*[@id='next2']")
    WebElement paginationNext;

    @FindBy(xpath="//*[@id='prev2']")
    WebElement paginationPrev;
    
    @FindBys(@FindBy(xpath="//*[@id='itemc']"))
    List <WebElement> productCategory; 
    
    @FindBy(xpath="//*[@id='tbodyid']/h2")
    WebElement productTitle;    
  
    @FindBy(xpath="//*[@id='tbodyid']/hr[2]")
    WebElement productPrice;    
  
    @FindBy(xpath="//*[@id='more-information']/p")
    WebElement productDescription;  
  
    @FindBy(xpath="//*[@id='tbodyid']/div[2]/div/a")
    WebElement addToCart;  
  
    @FindBy(xpath="//*[@id='cartur']")
    WebElement cart; 
 
    @FindBy(xpath="//*[@id='page-wrapper']/div/div[2]/button")
    WebElement cartPlaceOrder; 
    
    @FindBy(xpath="//*[@id='name']")
    WebElement nameField; 
  
    @FindBy(xpath="//*[@id='country']")
    WebElement countryField; 
  
    @FindBy(xpath="//*[@id='city']")
    WebElement cityField; 
  
    @FindBy(xpath="//*[@id='card']")
    WebElement cardField; 
  
    @FindBy(xpath="//*[@id='month']")
    WebElement monthField; 
  
    @FindBy(xpath="//*[@id='year']")
    WebElement yearField; 
  
    @FindBy(xpath="//*[@id='orderModal']/div/div/div[3]/button[2]")
    WebElement purchaseButton; 
    
    public PlaceOrder(WebDriver driver, Utility util){

        this.driver = driver;
        this.util = util;
        //This initElements method will create all WebElements

        PageFactory.initElements(driver, this);
    }
    
    //Click on sign-up links


  
    public void selectProductCategory(){
    	for(int i = 0; i < productCategory.size(); i++ ) {
        	String  prodCat = productCategory.get(i).getText();
        	if(!this.productCatgory.isEmpty() && this.productCatgory.equalsIgnoreCase(prodCat)) {
        		Utility.clickElement(productCategory.get(i), this.driver, this.util);
        		System.out.println("Category selected : " +prodCat); 
        		break;
        		
        	}
    	}


    }  
    
    public void selectProduct(){
    	String  prods;
    	boolean flag = false;
    	
    	for(int i = 0; i < products.size(); i++ ) {
    		prods = products.get(i).getText();

        	if(!this.productName.isEmpty() && this.productName.equalsIgnoreCase(prods)) {
        		Utility.clickElement(products.get(i), this.driver, this.util);
        		System.out.println("Product selected : " +prods); 
        		flag = true;
        		break;
        	}
	
    	}
    	
    	if(!flag) {
    		Utility.clickElement(paginationNext, this.driver, this.util);
    		
        	for(int i = 0; i < products.size(); i++ ) {
        		prods = products.get(i).getText();

            	if(!products.isEmpty() && this.productName.equalsIgnoreCase(prods)) {
            		Utility.clickElement(products.get(i), this.driver, this.util);
            		System.out.println("Product selected : " +prods); 
            		break;
            	}
    	
        	}
    	}

    } 
    public void goToCart(){
    	
    	Utility.clickElement(cart, this.driver, this.util);

    } 
    
    public void checkout(){
    	
    	Utility.clickElement(cartPlaceOrder, this.driver, this.util);

    } 
    
    public void set_name() throws IOException{
    	
    	Utility.sendKeys(nameField, this.driver, this.name, this.util);

    } 
    
    public void set_coutry() throws IOException{
    	
    	Utility.sendKeys(countryField, this.driver, this.country, this.util);

    } 
    
    public void set_city() throws IOException{
    	
    	Utility.sendKeys(cityField, this.driver, this.city, this.util);

    } 
    
    public void set_card_number() throws IOException{
    	
    	Utility.sendKeys(cardField, this.driver, this.CardNumber, this.util);

    } 
    
    public void set_card_month() throws IOException{
    	
    	Utility.sendKeys(monthField, this.driver, this.month, this.util);

    } 
    
    public void set_card_year() throws IOException{
    	
    	Utility.sendKeys(yearField, this.driver, this.year, this.util);

    } 
    
    public void click_purchase_button() throws InterruptedException, IOException{
    	
    	
    	Utility.clickElement(purchaseButton, this.driver, this.util);

    }  
    
    //Get the title of Login Page

    public String add_to_cart() throws InterruptedException, IOException{

    	return  Utility.handlePopUp(this.driver, this.util, addToCart);

    }
    
    /**

     * This POM method will be exposed in test case to login in the application

     * @param strUserName

     * @param strPasword

     * @return
     * @throws InterruptedException 
     * @throws IOException 

     */

    public void placeOrder(String name, String productCatgory, String productName, String country, String city, String CardNumber, String month, String year) throws InterruptedException, IOException{
    	this.name = name; 
    	this.productCatgory =productCatgory;
    	this.productName = productName;
    	this.country = country;
    	this.city = city;
    	this.CardNumber = CardNumber;
    	this.month = month;
    	this.year = year;
    	selectProductCategory();
    	//Fill select product
        this.selectProduct();        
       
        this.add_to_cart();
        goToCart();
        
        checkout();
        
        set_name();
        
        set_coutry();
        
        set_city();
        
        set_card_number();
        
        set_card_month();
        
        set_card_year();
        
        click_purchase_button();
        
    }
}
